#include <stdlib.h>
#include <stdio.h>

#include "./crypt.h"

void die() {
    printf("\
usage: sc (-e/-d) -k <key> -m (ascii | a | letter | l)\n\
  - '-e' to encrypt\n\
  - '-d' to decrypt\n\
    - if '-e' or '-d' are omitted mode defaults to '-e'\n\
  - '-k' to specify key\n\
    - default key is 3.1415 (or 'letsalllovelain' for letter mode)\n\
  - '-m' to specify method\n\
    - option 'ascii' (shorthand 'a') will perform full ascii shift\n\
      (\\x20 <SPACE> up to \\x7f '~')\n\
    - option 'letter' will perform letter shift\n\
      (\\x41 'A' to \\x5a 'Z' and \\x61 'a' to \\x7a 'z')\n\
sc stands for shiftcipher\n");
    exit(1);
}

#define NONE    0
#define KEY     1
#define METHOD  2
#define ASCII   1
#define LETTER -1

int isarg(char *arg, char *flag)
{
    if (strcmp(arg, flag)) return 0;
    return 1;
}

int main(int argc, char *argv[])
{
    if (argc < 2) die();
    /* scrapped, not portable */
    /* strcmp returns 0 on string equality, so in case of a correct flag */
    /* code evaluates to (argc >= 2 && !(!0 || !1)) */
    //if (argc >= 2 && !(!strcmp("-e", argv[1]) || !strcmp("-d", argv[1]))) die();
    //if (argc >= 3 && !(!!strcmp("-k", argv[2])) || !strcmp("-m", argv[2])) die();

    /* index, character */
    int i;
    char c;
    /* encrypt or decrypt */
    int e = 1;
    /* key */
    char *k = NULL;
    /* method */
    int m = ASCII;

    /* parse args (arg parsing is cancer) */
    int flag = NONE;
    for (int i = 1; i < argc; i++) {
        /* parse flags */
        if (isarg(argv[i], "-d")) { e = 0;         continue; };
        if (isarg(argv[i], "-k")) { flag = KEY;    continue; };
        if (isarg(argv[i], "-m")) { flag = METHOD; continue; };

        /* parse flag */
        if (flag == KEY || flag == METHOD) {
            if (flag == KEY) {
                k = argv[i];
                flag = NONE;
                printf("using custom key '%s'\n", k);
            }
            if (flag == METHOD) {
                if (isarg(argv[i], "letter") || isarg(argv[i], "l")) {
                    m = LETTER;
                }
                flag = NONE;
            }
        }
    }

    /* key (SUPER SECURE default xd) */
    if (k == NULL && m == LETTER) k = "letsalllovelain";
    if (k == NULL)                k = "3.1415";
    //printf("e %i k %s m %i\n", e, k, m);

    /* set correct cryptmethod */
    char (*cryptmethod)(char,char,int);
        cryptmethod = &cryptchar;
    if (m == LETTER) {
        /* check if key contains only ASCII letters */
        char *kp = k;
        while (*kp != '\0') {
            if (!isasciiletter(*kp)) {
                fprintf(stderr, "ERR: when using LETTER mode key must only contain ascii letters\n");
                return 1;
            }
            kp++;
        }
        cryptmethod = &cryptletter;
    }

    i = 0;
    /* iterate stdin */
    while ((c = getchar()) != -1) { //&& !feof(stdin)) {
        /* crypt */
        c = cryptmethod(c, k[i], e);
        printf("%c", c);

        /* reset i if cipher was fully iterated */
        if (k[++i] == 0) i = 0;
    }

    return 0;
}
