OPTIONS:=

sc: sc.c
	gcc -Wall -std=c99 -pedantic -g sc.c -o sc $(OPTIONS)

clean:
	rm -rf sc
