# shiftcipher

simple utility to encrypt ASCII characters like Caesar would've.

NOTE: the cipher was extended to not only encompass [a-zA-Z] but the whole range of ASCII characters, excluding control characters.

### usage

sc reads character by character from stdin and immediately outputs until EOF is signalled. as a consequence, all terminal input that contains multiple newlines yields funky output, as the terminal feeds the characters through stdin as soon as <Enter> is pressed. the utility is meant to be used in conjunction with the pipe:

`echo "test string MAKE IT SECURE 123456789!?" | ./sc -e`

to encrypt

`echo "(s%)1)(!z#x5\`O\\Y1^g.dYTjeS1ECHGCGKIN4Mz" | ./sc -e`
NOTE: be careful to escape characters when using shell like this xd

to decrypt

`echo "test string MAKE IT SECURE 123456789!?" | ./sc -e -k "customkeysBBY"`

to make use of a custom key

admittedly not the most elegant, functional or secure solution, but who cares

tested on linux only
for educational purposes
